terraform {
  required_version = ">= 0.13.4"
}

variable "execution_name" {
  type        = string
  default     = "no_name"
  description = "Execution the terraform module is associated with."
} 

variable "secret" {
  type        = string
  default     = "SUPER_SECRET_VALUE"
  description = "A super secret value, sensitive in terms of terraform."
}

variable "manager_name" {
  type        = string
  description = "Name of the controller manager"
}

variable "api_version" {
  type        = string
  description = "API version for controller managed api group."
}

variable "configured_controllers" {
  type        = string
  description = "Controllers that are configured with the manager."
}

variable "env" {
  type        = string
  description = "Environment we are running in."
}

variable "module_index" {
  type        = number
  description = "Index of the module."
}

variable "supported_variable_types" {
  type        = list(string)
  description = "Variables type supported for input variable to the terraform module."
}

variable "dependencies" {
  type        = map(string)
  default     = {}
  description = "A list of execution that this execution depends on."
}

// Output variables for the module

output "hello_world" {
  value       = <<EOT
Hello, World!
From ${var.manager_name} running ${var.api_version} - index(${var.module_index})

Configured controllers: ${var.configured_controllers}
Environment: ${var.env}
Supported Variable Types: ${join(", ", var.supported_variable_types)}
EOT
  description = "Output of the hello-world module"
}

output "execution_name" {
  value       = var.execution_name
}

output "secret" {
  sensitive = true
  value     = var.secret
}

output "dependencies" {
  value = var.dependencies
}